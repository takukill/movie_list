package com.movie.asahina.movie;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.movie.asahina.movie.preferences.PreferencesModule;
import com.movie.asahina.movie.ui.main.MainActivity;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * カスタムしたアプリケーション。
 */
public class BaseApplication extends MultiDexApplication {

	public static final String TAG = BaseApplication.class.getSimpleName();

	/**
	 * Application Context をどこでも取得できるようにするため、BaseApplication 自体を static で保持する。
	 */
	private static BaseApplication instance = null;

	public static BaseComponent getComponent() {
		return instance.daggerComponent;
	}

	public static Context getContext() {
		return instance.getApplicationContext();
	}

	public static String version;

	public static String appVersion;

	/**
	 * Dagger2のcomponent
	 */
	private BaseComponent daggerComponent;

	private boolean mIsForeground = false;

	@Override
	public void onCreate() {
		super.onCreate();

		version = Build.VERSION.RELEASE;

		try {
			PackageManager packageManager = getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
			appVersion = packageInfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}


		/**
		 * 強制終了時の後処理
		 * @see <a href="http://kokufu.blogspot.jp/2013/01/android-uncaughtexceptionhandler-anr.html">
		 *     UncaughtExceptionHandler の使い方</a>
		 */
		// 現在設定されている UncaughtExceptionHandler を退避
		final Thread.UncaughtExceptionHandler savedUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		// キャッチされなかった例外発生時の処理を設定する
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

			// 無限ループ防止フラグ
			private volatile boolean isCrashing = false;

			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				try {
					if (!isCrashing) {
						isCrashing = true;
						Log.d(TAG, "uncaughtException :" + ex);

						// アプリ再起動
						Intent intent = new Intent(BaseApplication.this, MainActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(intent);
					}
				} finally {
					savedUncaughtExceptionHandler.uncaughtException(thread, ex);
				}
			}
		});


		daggerComponent = DaggerBaseComponent.builder()
				.preferencesModule(new PreferencesModule(this))
				.build();

		JodaTimeAndroid.init(this);

		setInstance(this);
//		this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacksImpl());
	}

	protected void setInstance(BaseApplication application) {
		instance = application;
	}

	public boolean isForeground() {
		return mIsForeground;
	}

	/**
	 * アプリケーションの表示状態が変わった
	 *
	 * @param isForeground
	 */
	private void setForeground(boolean isForeground) {
		mIsForeground = isForeground;
	}

	/**
	 //	 * アプリケーションの表示状態を監視する
	 //	 * <p>
	 //	 * see http://yslibrary.net/2015/07/30/android_how_to_detect_app_is_background_or_not/
	 //	 */
//	private class ActivityLifecycleCallbacksImpl implements Application.ActivityLifecycleCallbacks {
//		private int running = 0;
//
//		@Override
//		public void onActivityCreated(Activity activity, Bundle bundle) {
//		}
//
//		@Override
//		public void onActivityStarted(Activity activity) {
//			running++;
//			if (running == 1) {
//				setForeground(true);
//			}
//		}
//
//		@Override
//		public void onActivityResumed(Activity activity) {
//		}
//
//		@Override
//		public void onActivityPaused(Activity activity) {
//		}
//
//		@Override
//		public void onActivityStopped(Activity activity) {
//			running--;
//			if (running == 0) {
//				setForeground(false);
//			}
//		}
//
//		@Override
//		public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
//		}
//
//		@Override
//		public void onActivityDestroyed(Activity activity) {
//		}
//	}
}
