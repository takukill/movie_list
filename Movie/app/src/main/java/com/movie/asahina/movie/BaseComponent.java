package com.movie.asahina.movie;


import com.movie.asahina.movie.model.MovieModel;
import com.movie.asahina.movie.model.module.ModelModule;
import com.movie.asahina.movie.preferences.PreferencesModule;
import com.movie.asahina.movie.rest.api.module.ApiModule;
import com.movie.asahina.movie.ui.main.favorite.FavoriteListViewModel;
import com.movie.asahina.movie.ui.main.list.MovieListViewModel;
import com.movie.asahina.movie.ui.main.search.SearchViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
		ModelModule.class,
		PreferencesModule.class,
		ApiModule.class
})
public interface BaseComponent {

	// Model
	void inject(MovieModel model);

	// ViewModel
	void inject(MovieListViewModel viewModel);
	void inject(SearchViewModel viewModel);
	void inject(FavoriteListViewModel viewModel);
}
