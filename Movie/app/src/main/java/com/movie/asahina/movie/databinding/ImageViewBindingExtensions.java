package com.movie.asahina.movie.databinding;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.widget.ImageView;

import com.movie.asahina.movie.BuildConfig;
import com.squareup.picasso.Picasso;


public class ImageViewBindingExtensions {

	//region ImageView Extensions
	//region Base
	@BindingAdapter("imageResource")
	public static void setImageResource(ImageView imageView, @DrawableRes int resourceId) {
		imageView.setImageResource(resourceId);
	}

	@BindingAdapter("imageBitmap")
	public static void setImageBitmap(ImageView imageView, Bitmap bitmap) {
		if (bitmap == null) {
			return;
		}

		imageView.setImageBitmap(bitmap);
	}

	@BindingAdapter("imageUrl")
	public static void setImageUrl(ImageView imageView, String urlOrUri) {
		if (TextUtils.isEmpty(urlOrUri)) {
			return;
		}

		Picasso picasso = Picasso.with(imageView.getContext());
		if (BuildConfig.DEBUG) {
			picasso.setIndicatorsEnabled(true);
		}
		picasso.load(urlOrUri).into(imageView);
	}

	@BindingAdapter({"imageBitmap", "imageUrl"})
	public static void setImageBitmapOrUrl(ImageView imageView, Bitmap bitmap, String urlOrUri) {
		if (bitmap != null) {
			setImageBitmap(imageView, bitmap);
		} else {
			setImageUrl(imageView, urlOrUri);
		}
	}
	//endregion

	@BindingAdapter("imageTint")
	public static void setImageTint(ImageView imageView, @ColorInt int colorInt) {
		Drawable drawable = imageView.getDrawable();
		drawable.setColorFilter(colorInt, PorterDuff.Mode.SRC_IN);
		imageView.setImageDrawable(drawable);
	}

	@BindingAdapter("imageTintString")
	public static void setImageTint(ImageView imageView, String colorString) {
		int color = Color.parseColor(colorString);

		Drawable drawable = imageView.getDrawable();
		drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
		imageView.setImageDrawable(drawable);
	}

	@BindingAdapter("colorFilter")
	public static void setColorFilterString(ImageView imageView, String colorString) {
		int color = Color.parseColor(colorString);
		imageView.setColorFilter(color, PorterDuff.Mode.SRC_IN);
	}
	//endregion
}
