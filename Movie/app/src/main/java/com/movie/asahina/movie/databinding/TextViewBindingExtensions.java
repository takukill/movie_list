package com.movie.asahina.movie.databinding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;


public class TextViewBindingExtensions {

	//region TextView Extensions
	@BindingAdapter("linkMovementEnable")
	public static void setLinkMovementEnable(TextView textView, boolean enable) {
		if (enable) {
			textView.setMovementMethod(LinkMovementMethod.getInstance());
		} else {
			textView.setMovementMethod(null);
		}
	}

	@BindingAdapter("inputRightText")
	public static void setInputGravity(EditText editText, String text) {
		if (TextUtils.isEmpty(text)) {
			editText.setGravity(Gravity.CENTER_VERTICAL);
		} else {
			Paint p = new Paint();
			p.setTextSize(editText.getTextSize());
			int width = (int) p.measureText(text);
			editText.setPadding(0, 0, width, 0);
			editText.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		}
	}

	@BindingAdapter("inputNumber")
	public static void setInputNumber(final EditText editText, boolean flag) {
		if (flag) {
			editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		} else {
			editText.setInputType(InputType.TYPE_CLASS_TEXT);
		}
	}

	@BindingAdapter("inputPassword")
	public static void setInputPassword(final EditText editText, boolean flag) {
		if (flag) {
			editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		} else {
			editText.setInputType(InputType.TYPE_CLASS_TEXT);
		}
	}

	@BindingAdapter({"inputNumber", "inputPassword"})
	public static void setInputType(final EditText editText, boolean number, boolean password) {
		if (number) {
			if (password) {
				editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
			} else {
				editText.setInputType(InputType.TYPE_CLASS_NUMBER);
			}
		} else if (password) {
			editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		} else {
			editText.setInputType(InputType.TYPE_CLASS_TEXT);
		}
	}

	@BindingAdapter("maxLength")
	public static void setMaxLength(final EditText editText, Integer maxTextLength) {
		if (maxTextLength == null) {
			return;
		}

		InputFilter.LengthFilter lengthFilter = new InputFilter.LengthFilter(maxTextLength);
		editText.setFilters(new InputFilter[]{lengthFilter});
	}

//	@BindingAdapter({"linkWeb"})
//	public static void setLinkWeb(TextView view, boolean linkWeb) {
//		if (linkWeb) {
//			Context context = view.getContext();
//			view.setAutoLinkMask(Linkify.WEB_URLS);
//			view.setLinkTextColor(context.getResources().getColor(R.color.colorLink));
//			view.setMovementMethod(ChatLinkMovementMethod.getInstance(new ChatLinkMovementMethod.OnClickLink() {
//				@Override
//				public void selectLink(String url) {
//					WebViewActivity.startActivity(context, url);
//				}
//			}));
//		}
//	}
//
//	@BindingAdapter({"onePointLinkPattern", "onePointLink"})
//	public static void setLinkWebPink(TextView view, String onePointLinkPattern, String onePointLink) {
//		Context context = view.getContext();
//		Pattern pattern = Pattern.compile(onePointLinkPattern);
//		Linkify.addLinks(view, pattern, onePointLink);
//		view.setLinkTextColor(context.getResources().getColor(R.color.watermelon_ff3779));
//		view.setMovementMethod(ChatLinkMovementMethod.getInstance(new ChatLinkMovementMethod.OnClickLink() {
//			@Override
//			public void selectLink(String url) {
//				ContactUsActivity.startActivity(context);
//			}
//		}));
//	}

	@BindingAdapter({"bold"})
	public static void setBold(TextView view, boolean bold) {
		if (bold) {
			view.setTypeface(Typeface.DEFAULT_BOLD);
		} else {
			view.setTypeface(Typeface.DEFAULT);
		}
	}
//endregion
}


