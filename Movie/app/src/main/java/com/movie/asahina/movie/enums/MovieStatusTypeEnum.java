package com.movie.asahina.movie.enums;

import android.support.annotation.ColorInt;

import com.movie.asahina.movie.R;

/**
 * Created by takuasahina on 2018/04/11.
 */

public enum MovieStatusTypeEnum {

	DEFAULT(0, "特になし", R.color.white),

	FAVORITE(1, "お気に入り", R.color.red100),

	LINE_1(2, "ライン１", R.color.green100),

	LINE_2(3, "ライン２", R.color.blue100),

	DELETE(4, "削除", R.color.white);


	MovieStatusTypeEnum(int code, String label, int backgroundColorInt) {
		this.code = code;
		this.label = label;
		this.backgroundColorInt = backgroundColorInt;
	}


	public int code;

	public String label;

	@ColorInt
	public final int backgroundColorInt;
}
