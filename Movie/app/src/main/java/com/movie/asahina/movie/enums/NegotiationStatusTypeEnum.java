//package com.movie.asahina.movie.enums;
//
//import android.support.annotation.DrawableRes;
//
//import com.google.gson.annotations.SerializedName;
//
//import jp.co.carview.qiilo.R;
//
///**
// * Created by takuasahina on 2017/11/09.
// */
//
//public enum NegotiationStatusTypeEnum {
//
//	@SerializedName("0")
//	NONE(00, "商談前", R.drawable.chat_panel_1_active, R.drawable.chat_panel_2, R.drawable.chat_panel_3, null, null, null, null),
//
//	@SerializedName("10")
//	CONDITIONS(10, "条件確定【10】", R.drawable.chat_panel_1_active, R.drawable.chat_panel_2, R.drawable.chat_panel_3, "商談がまとまったら、取引相手への売却条件を確定してください。", "相手の売却条件確定をお待ちください。", "売却条件確定", "売却条件確認"),
//
//	@SerializedName("20")
//	PURCHASE_1(20, "購入手続【20】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_active, R.drawable.chat_panel_3, "取引相手の購入確定をお待ちください。", "購入手続を行ってください。", "売却条件取消", "購入手続"),
//
//	@SerializedName("21")
//	PURCHASE_2(21, "購入手続（コンビニ、ペイジー）【21】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_active, R.drawable.chat_panel_3, "取引相手の購入確定をお待ちください。", "コンビニで代金を支払ってください。", null, "支払情報確認"),
//
//	@SerializedName("30")
//	VEHICLE_DELIVERY(30, "車両受渡【30】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_finished, R.drawable.chat_panel_3_active, "取引相手に車両と車検証などの書類を引き渡してください。", "車と車検証を受取り、受取完了報告ボタンを押してください。", null, "受取完了報告"),
//
//	@SerializedName("40")
//	PARTNER_EVALUATION(40, "相手評価待ち 【40】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_finished, R.drawable.chat_panel_3_finished, "取引が完了しました。取引相手の評価を行ってください。", "取引が完了しました。取引相手の評価を行ってください。", "取引相手を評価", "取引相手を評価"),
//
//	@SerializedName("41")
//	APP_EVALUATION(41, "アプリ評価待ち【41】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_finished, R.drawable.chat_panel_3_finished, "取引が完了しました。アプリの評価を行ってください。", "取引が完了しました。アプリの評価を行ってください。", "アプリを評価", "アプリを評価"),
//
//	@SerializedName("50")
//	CANCEL(50, "購入後キャンセル【50】", R.drawable.chat_panel_1_finished, R.drawable.chat_panel_2_finished, R.drawable.chat_panel_3_finished, "取引がキャンセルされました。", "取引がキャンセルされました。", null, null);
//
//
//	NegotiationStatusTypeEnum(int code, String label, @DrawableRes int chatPanel1, @DrawableRes int chatPanel2, @DrawableRes int chatPanel3, String sellerLabel, String buyerLabel, String sellerPrimary, String buyerPrimary) {
//		this.code = code;
//		this.label = label;
//		this.chatPanel1 = chatPanel1;
//		this.chatPanel2 = chatPanel2;
//		this.chatPanel3 = chatPanel3;
//		this.sellerLabel = sellerLabel;
//		this.buyerLabel = buyerLabel;
//		this.sellerPrimary = sellerPrimary;
//		this.buyerPrimary = buyerPrimary;
//	}
//
//
//	public final int code;
//
//	public final String label;
//
//	@DrawableRes
//	public final int chatPanel1;
//
//	@DrawableRes
//	public final int chatPanel2;
//
//	@DrawableRes
//	public final int chatPanel3;
//
//	private final String sellerLabel;
//
//	private final String buyerLabel;
//
//	private final String sellerPrimary;
//
//	private final String buyerPrimary;
//
//
//	public String getDescriptionText(boolean seller) {
//		return seller ? sellerLabel : buyerLabel;
//	}
//
//	public String getPrimaryText(boolean seller) {
//		return seller ? sellerPrimary : buyerPrimary;
//	}
//
//}
