package com.movie.asahina.movie.model;


import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.movie.asahina.movie.model.base.BaseModel;
import com.movie.asahina.movie.model.preference.PreferencesModel;
import com.movie.asahina.movie.preferences.dto.MovieItem;

import java.util.List;

/**
 * Created by takuasahina on 2017/12/14.
 */

public class MovieModel extends BaseModel {

	public MovieModel(PreferencesModel preferencesModel) {

		this.preferencesModel = preferencesModel;
	}


	private final PreferencesModel preferencesModel;


	//region FavoriteList
	public static final String KEY_FAVORITE_LIST = "KEY_FAVORITE_LIST";

	public List<MovieItem> getFavoriteList() {
		return preferencesModel.getFavoriteList();
	}

	public void setFavoriteList(List<MovieItem> list) {
		preferencesModel.setFavoriteList(list);
		firePropertyChange(KEY_FAVORITE_LIST);
	}

	public void addFavoriteList(MovieItem item) {
		preferencesModel.addFavoriteList(item);
		firePropertyChange(KEY_FAVORITE_LIST);
	}

	public void removeFavoriteItem(int position) {
		preferencesModel.removeFavoriteList(position);
		firePropertyChange(KEY_FAVORITE_LIST);
	}

	public boolean isRegisterFavoriteItem(String title) {
		return Stream.of(preferencesModel.getFavoriteList())
				.filter(new Predicate<MovieItem>() {
					@Override
					public boolean test(MovieItem value) {
						return value.label.equals(title);
					}
				})
				.findFirst()
				.orElse(null)
				!= null;
	}
	//endregion

	//region movieList
	public static final String KEY_MOVIE_LIST = "KEY_MOVIE_LIST";

	public List<MovieItem> getMovieList() {
		return preferencesModel.getMovieList();
	}

	public void setMovieList(List<MovieItem> list) {
		preferencesModel.setMovieList(list);
		firePropertyChange(KEY_MOVIE_LIST);
	}

	public void addMovieList(MovieItem item) {
		preferencesModel.addMovieList(item);
		firePropertyChange(KEY_MOVIE_LIST);
	}

	public void removeItem(int position) {
		preferencesModel.removeMovieList(position);
		firePropertyChange(KEY_MOVIE_LIST);
	}

	public boolean isRegisterItem(String title) {
		return Stream.of(preferencesModel.getMovieList())
				.filter(new Predicate<MovieItem>() {
					@Override
					public boolean test(MovieItem value) {
						return value.label.equals(title);
					}
				})
				.findFirst()
				.orElse(null)
				!= null;
	}
	//endregion
}


