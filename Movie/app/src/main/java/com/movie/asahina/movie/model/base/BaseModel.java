package com.movie.asahina.movie.model.base;

import com.movie.asahina.movie.rest.base.HttpError;

import java.beans.PropertyChangeSupport;

/**
 * Base Model
 * プロパティ変更通知の仕組みを提供します。
 * <p>
 * ここに機能を追加する場合は、事前にチーム内で相談してください。
 */
public abstract class BaseModel {

	protected BaseModel() {
		listeners = new PropertyChangeSupport(this);
	}


	private PropertyChangeSupport listeners;


	public void addPropertyChangeListener(PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		listeners.removePropertyChangeListener(listener);
	}

	protected void firePropertyChange(String propertyName) {
		listeners.firePropertyChange(propertyName, null, null);
	}


	// region HTTPエラー
	public static final String PROPERTY_HTTP_ERROR = "PROPERTY_HTTP_ERROR";

	private HttpError httpError;

	public HttpError getHttpError() {
		return httpError;
	}

	public void setHttpError(HttpError httpError) {
		if (this.httpError == httpError) {
			return;
		}

		this.httpError = httpError;
		firePropertyChange(PROPERTY_HTTP_ERROR);
	}
	//endregion
}
