package com.movie.asahina.movie.model.base;

import java.beans.PropertyChangeEvent;

/**
 * プロパティ変更リスナー
 */

public abstract class PropertyChangeListener implements java.beans.PropertyChangeListener {

    public abstract void propertyChange(String propertyName);

    @Override
    public final void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        propertyChange(propertyChangeEvent.getPropertyName());
    }
}
