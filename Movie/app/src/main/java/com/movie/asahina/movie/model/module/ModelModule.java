package com.movie.asahina.movie.model.module;

import com.movie.asahina.movie.model.MovieModel;
import com.movie.asahina.movie.model.preference.PreferencesModel;
import com.movie.asahina.movie.preferences.Preferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger2で注入するオブジェクトを指定する。
 */

@Module
public class ModelModule {


	@Provides
	@Singleton
	public PreferencesModel providePreferencesModel(Preferences preferences) {
		return new PreferencesModel(preferences);
	}




	@Provides
	@Singleton
	public MovieModel provideMovieModel(PreferencesModel preferencesModel) {
		return new MovieModel(preferencesModel);
	}
}