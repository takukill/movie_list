package com.movie.asahina.movie.model.preference;

import com.movie.asahina.movie.model.base.BaseModel;
import com.movie.asahina.movie.preferences.Preferences;
import com.movie.asahina.movie.preferences.dto.MovieItem;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * SfxPreference の変更を管理する Model。
 */
@Singleton
public class PreferencesModel extends BaseModel {

	@Inject
	public PreferencesModel(Preferences preferences) {
		this.preferences = preferences;
	}


	private final Preferences preferences;


	//region FavoriteList
	private static final String KEY_FAVORITE_LIST = "KEY_FAVORITE_LIST";

	public List<MovieItem> getFavoriteList() {
		return preferences.getFavoriteList();
	}

	public void setFavoriteList(List<MovieItem> list) {
		if (getFavoriteList() == list) {
			return;
		}

		preferences.setFavoriteList(list);
		firePropertyChange(KEY_FAVORITE_LIST);
	}

	public void addFavoriteList(MovieItem item) {
		if (getFavoriteList().contains(item)) {
			return;
		}

		List<MovieItem> list = getFavoriteList();
		list.add(item);
		setFavoriteList(list);
	}

	public void removeFavoriteList(int position) {
		List<MovieItem> list = getFavoriteList();
		list.remove(position);
		setFavoriteList(list);
	}
	//endregion

	//region MovieList
	private static final String KEY_MOVIE_LIST = "KEY_MOVIE_LIST";

	public List<MovieItem> getMovieList() {
		return preferences.getMovieList();
	}

	public void setMovieList(List<MovieItem> list) {
		if (getMovieList() == list) {
			return;
		}

		preferences.setMovieList(list);
		firePropertyChange(KEY_MOVIE_LIST);
	}

	public void addMovieList(MovieItem item) {
		if (getMovieList().contains(item)) {
			return;
		}

		List<MovieItem> list = getMovieList();
		list.add(item);
		setMovieList(list);
	}

	public void removeMovieList(int position) {
		List<MovieItem> list = getMovieList();
		list.remove(position);
		setMovieList(list);
	}
	//endregion
}