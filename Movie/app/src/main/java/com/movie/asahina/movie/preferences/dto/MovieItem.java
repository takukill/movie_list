package com.movie.asahina.movie.preferences.dto;

import android.databinding.ObservableField;

import com.movie.asahina.movie.enums.MovieStatusTypeEnum;

/**
 * Created by takuasahina on 2018/02/28.
 */

public class MovieItem {

	public MovieItem(String label, String url, String year, String rating, String thumbnail) {
		this.movieStatusTypeEnum.set(MovieStatusTypeEnum.DEFAULT);
		this.label = label;
		this.url = url;
		this.year = year;
		this.rating = rating;
		this.thumbnail = thumbnail;
	}


	public final String label;

	public final String url;

	public final String rating;

	public final String year;

	public final String thumbnail;

	public ObservableField<MovieStatusTypeEnum> movieStatusTypeEnum = new ObservableField<>();


	public void setMovieStatusTypeEnum(MovieStatusTypeEnum movieStatusTypeEnum) {
		if (this.movieStatusTypeEnum == null) {
			this.movieStatusTypeEnum = new ObservableField<>();
		}

		this.movieStatusTypeEnum.set(movieStatusTypeEnum);
	}
}
