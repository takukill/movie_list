package com.movie.asahina.movie.rest.api;

import android.support.annotation.NonNull;

import com.movie.asahina.movie.rest.base.AbstractHttpApi;
import com.movie.asahina.movie.rest.base.ApiConsts;
import com.movie.asahina.movie.rest.base.HttpError;
import com.movie.asahina.movie.rest.dto.request.AccountsPostParam;
import com.movie.asahina.movie.rest.dto.response.AccountPost;

import org.jdeferred.Promise;

import javax.inject.Singleton;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;


@Singleton
public class AccountPostApi extends AbstractHttpApi {

	public
	@NonNull
	Promise<AccountPost, HttpError, Void> request(AccountsPostParam param) {
		Call call = retrofit.create(Api.class).execute(param);
		return setCallback(call);
	}

	public interface Api {
		@Headers("X-HTTP-Method-Override: GET")
		@POST(ApiConsts.ACCOUNT)
		Call<AccountPost> execute(@Body AccountsPostParam param);
	}
}

