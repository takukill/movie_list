package com.movie.asahina.movie.rest.base;

/**
 * API定義
 */
public class ApiConsts {

	public static final String BASE_URL = "http://qiilo-api.rancher.dev.carview.co.jp";

	//	検証環境：https://pt01.mul-pay.jp/ext/api/credit/getToken
//	本番環境：https://p01.mul-pay.jp/ext/api/credit/getToken
	public static final String GMO_BASE_URL = "https://pt01.mul-pay.jp";

	//region 商品
	// 商品情報仮登録API
	public static final String TEMPORARY = BASE_URL + "/v1/items/temporary";

	// 商品画像仮アップロードAPI
	public static final String PICTURES = BASE_URL + "/v1/items/pictures";

	// 商品情報本登録AP
	public static final String ITEMS = BASE_URL + "/v1/items";

	// 商品情報登録（マスタ）API
	public static final String SHOP_RANKING_URL = BASE_URL + "/v1/Products";

	// 車両情報判別API
	public static final String REFERENCE_ITEM = BASE_URL + "/v1/reference_item";

	// 商品リスト取得API
	public static final String ITEMS_LIST = BASE_URL + "/v1/items";

	// 検索結果カウント取得API
	public static final String COUNT_BY_CATEGORY = BASE_URL + "/v1/items/count_by_category";

	// 商品詳細取得API
	public static final String ITEMS_SHOW = BASE_URL + "/v1/items/show";

	// 検索条件登録API
	public static final String SEARCH_CONDITION = BASE_URL + "/v1/search_condition";

	// 検索条件リスト取得API

	// 検索条件削除API

	//endregion


	//region 商談
	//いいね登録更新API
	public static final String LIKE = BASE_URL + "/v1/like";

	// オープンコメント登録API
	public static final String COMMENT = BASE_URL + "/v1/comments";

	// オープンコメントリスト取得API
	public static final String COMMENT_LIST = BASE_URL + "/v1/comments";

	//チャットリスト取得API
	public static final String NEGOTIATIONS_MESSAGES = BASE_URL + "/v1/negotiations/messages";

	//チャット送信
	public static final String NEGOTIATIONS_MESSAGE_POST = BASE_URL + "/v1/negotiations/message";

	//チャット送信
	public static final String NEGOTIATIONS_MESSAGE_GET = BASE_URL + "/v1/negotiations/message";

	//チャット既読
	public static final String NEGOTIATIONS_MESSAGE_PUT = BASE_URL + "/v1/negotiations/message";

	//商談部屋作成API
	public static final String NEGOTIATIONS_STATUS_POST = BASE_URL + "/v1/negotiations/status";

	//商談部屋作成API
	public static final String NEGOTIATIONS_STATUS_GET = BASE_URL + "/v1/negotiations/status";

	//商談部屋リスト取得API
	public static final String NEGOTIATIONS = BASE_URL + "/v1/negotiations";

	//クレジットカード決済API
	public static final String PAYMENT_CREDIT_CARD = BASE_URL + "/v1/payment/credit_card";

	// コンビニ/ペイジー決済API
	public static final String PAYMENT_MULTI = BASE_URL + "/v1/payment/multi";

	// GMO結果通知プログラム用エンドポイント
	public static final String PAYMENT_RESULT = BASE_URL + "/v1/payment/result";

	// 売却条件確定API
	public static final String SELLING_CONDITION_POST = BASE_URL + "/v1/selling/condition";

	// 売却条件取得API
	public static final String SELLING_CONDITION_GET = BASE_URL + "/v1/selling/condition";

	// 支払情報取得API
	public static final String PAYMENT = BASE_URL + "/v1/payment";

	// 売却条件取消API
	public static final String NEGOTIATIONS_CANCELLATION = BASE_URL + "/v1/negotiations/cancellation";

	// 受取完了API
	public static final String NEGOTIATIONS_DELIVERY = BASE_URL + "/v1/negotiations/delivery";

	// 受取完了API
	public static final String EVALUATIONS = BASE_URL + "/v1/evaluations";

	//評価登録API
	public static final String EVALUATION = BASE_URL + "/v1/evaluation";

	public static final String APPOINTMENT_GET = BASE_URL + "/v1/appointment";

	//現車（場所）登録更新APi
	public static final String APPOINTMENT_SPOT = BASE_URL + "/v1/appointment/spot";

	//現車（時間）登録更新APi
	public static final String APPOINTMENT_SCHEDULE = BASE_URL + "/v1/appointment/schedule";
	//endregion

	//region 売上振込買取
	// 売上リスト取得API
	public static final String EARNINGS = BASE_URL + "/v1/earnings";

	// 振込申請リスト取得API
	public static final String TRANSFERS_HISTORY = BASE_URL + "/v1/transfers/history";

	// 振込申請登録API
	public static final String TRANSFERS = BASE_URL + "/v1/transfers";

	// 購入リスト取得API
	public static final String PURCHASE_HISTORY = BASE_URL + "/v1/purchase/history";

	// 買取申請仮登録API
	public static final String ADMIN_PURCHASE_TEMPORARY = BASE_URL + "/v1/admin_purchase/temporary";

	// 買取申請画像仮アップロードAPI
	public static final String ADMIN_PURCHASE_PICTURE = BASE_URL + "/v1/admin_purchase/pictures";

	// 買取申請本登録API
	public static final String ADMIN_PURCHASE = BASE_URL + "/v1/admin_purchase";

	//endregion

	//region ユーザー
	//仮ユーザー登録
	public static final String USER_TEMPORARY = BASE_URL + "/v1/user/temporary";

	//SMS送信API
	public static final String SMS = BASE_URL + "/v1/sms";

	//SMS認証API
	public static final String SMS_VERIFICATION = BASE_URL + "/v1/sms/verification";

	//新規ユーザー登録API
	public static final String USER = BASE_URL + "/v1/user";

	//データ引継ぎAPI
	public static final String DATA_TRANSFER = BASE_URL + "/v1/data_transfer";

	// プロフィール
	public static final String PROFILE_SHOW = BASE_URL + "/v1/profile/show";

	//プロフィール更新API
	public static final String PROFILE = BASE_URL + "/v1/profile";

	// TBD:通知登録API
	public static final String NOTIFICATION = BASE_URL + "/v1/notifications";

	//ユーザーブロック登録更新API
	public static final String BLOCK = BASE_URL + "/v1/users/block";

	//通報登録API
	public static final String REPORT = BASE_URL + "/v1/report";
	//endregion


	//region マイメニュー
	public static final String PUSH_SETTING = BASE_URL + "/v1/push_settings";

	public static final String ACCOUNT = BASE_URL + "/v1/accounts";

	public static final String RESIGNATION = BASE_URL + "/v1/resignation";

	public static final String CONTACT = BASE_URL + "/v1/contact";
	//endregion


	//region マスタ
	// マスター
	public static final String CAR_LINE_MASTERS = BASE_URL + "/v1/car_line_masters";

	// マスタ更新リスト取得API
	public static final String CHECK = BASE_URL + "/v1/masters/updates";

	// マスタ取得API
	public static final String MASTER = BASE_URL + "/v1/masters";

	// 金融機関マスタ取得API
	public static final String BankMasters = BASE_URL + "/v1/bank_masters";
	//endregion


	public static final String GMO_CREDIT_CARD_TOKEN = GMO_BASE_URL + "/ext/api/credit/getToken";

	public static final String YAHOO_BASE = "https://map.yahooapis.jp";

	public static final String YAHOO_GEO_CODE = YAHOO_BASE + "/geocode/V1/geoCoder";
}
