package com.movie.asahina.movie.rest.base;

import android.databinding.ObservableArrayMap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * REST取得のエラー
 */

public class HttpError {
	/**
	 * エラーのステータス
	 */
	@NonNull
	public final ResponseStatusEnum status;

	/**
	 * エラー時のレスポンス
	 * CONNECTION_ERRとDATA_ERR時は、null
	 */
	@Nullable
	public final CommonResponse response;

	/**
	 * 共通のエラーメッセージを返す。
	 * 画面個別のエラーメッセージは、個別に実装してください。
	 *
	 * @return
	 */
	public ObservableArrayMap<String, String> getErrorMessage() {
		ObservableArrayMap<String, String> message = new ObservableArrayMap<>();

		switch (status) {
			case BODY_ERROR:
			case CERTIFICATION_ERROR:
			case REQUEST_ERROR:
			case UPGRADE_ERROR:
			case SERVER_CONNECTION_ERROR:
			case SERVER_MAINTENANCE_ERROR:
				message = response.getErrorMessage();
				break;
		}

		return message;
	}

	public HttpError(@NonNull ResponseStatusEnum status, @Nullable CommonResponse response) {
		this.status = status;
		this.response = response;
	}
}
