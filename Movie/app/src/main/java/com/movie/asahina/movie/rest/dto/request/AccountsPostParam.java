package com.movie.asahina.movie.rest.dto.request;

/**
 * Created by yutowatanabe on 2017/11/30.
 */

public class AccountsPostParam {

	public AccountsPostParam(String current_password, String new_password) {
		this.current_password = current_password;
		this.new_password = new_password;
	}

	//	{
//		"current_password": "ArpxoqXB12",
//			"new_password": "308PsgeR"
//	}

	public String current_password;

	public String new_password;
}