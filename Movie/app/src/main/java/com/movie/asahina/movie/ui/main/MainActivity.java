package com.movie.asahina.movie.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.databinding.ActivityMainBinding;
import com.movie.asahina.movie.ui.main.favorite.FavoriteListFragment;
import com.movie.asahina.movie.ui.main.list.MovieListFragment;
import com.movie.asahina.movie.ui.main.search.SearchFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by takuasahina on 2018/02/27.
 */

public class MainActivity extends AppCompatActivity {

	public static void startActivity(Activity activity) {
		activity.startActivity(new Intent(activity, MainActivity.class));
	}


	private ActivityMainBinding binding;

	private MainViewModel viewModel;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		viewModel = new MainViewModel();
		binding.setViewModel(viewModel);
		binding.setView(this);


		List<Fragment> fragments = new ArrayList<>();

		Stream.of(TabItemTypeEnum.values())
				.forEach(new Consumer<TabItemTypeEnum>() {
					@Override
					public void accept(TabItemTypeEnum tabItemTypeEnum) {
						binding.tabLayout.addTab(binding.tabLayout.newTab().setText(tabItemTypeEnum.title));
						fragments.add(tabItemTypeEnum.fragment);
					}
				});

		binding.viewPager.setAdapter(new ListPagerAdapter(getSupportFragmentManager(), fragments));

		binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
		binding.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				binding.viewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
			}
		});
		binding.viewPager.setCurrentItem(1);

		setSupportActionBar(binding.toolbar);
	}


	@Override
	public void onBackPressed() {
		int position = binding.viewPager.getCurrentItem();
		if (position == TabItemTypeEnum.SEARCH.code) {
			((SearchFragment) (((ListPagerAdapter) binding.viewPager.getAdapter()).fragments.get(position))).onBackPressed();
			return;
		}

		super.onBackPressed();
	}


	//region PagerAdapter
	private class ListPagerAdapter extends FragmentStatePagerAdapter {

		private final List<Fragment> fragments;

		public ListPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = new ArrayList<>(fragments);
		}

		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
	}
	//endregion

	private enum TabItemTypeEnum {

		FAVORITE(0, "お気に入り", FavoriteListFragment.newInstance()),

		LIST(1, "見たいリスト", MovieListFragment.newInstance()),

		SEARCH(2, "検索", SearchFragment.newInstance());


		TabItemTypeEnum(int code, String title, Fragment fragment) {
			this.code = code;
			this.title = title;
			this.fragment = fragment;
		}


		public int code;

		public String title;

		public Fragment fragment;
	}
}
