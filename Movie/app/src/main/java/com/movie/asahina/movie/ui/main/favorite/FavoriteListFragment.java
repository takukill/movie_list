package com.movie.asahina.movie.ui.main.favorite;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.movie.asahina.movie.BR;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.databinding.FragmentFavoriteListBinding;
import com.movie.asahina.movie.enums.MovieStatusTypeEnum;
import com.movie.asahina.movie.preferences.dto.MovieItem;
import com.movie.asahina.movie.view.dialog.SmartDialogUtils;
import com.movie.asahina.movie.view.recyclerview.DataBindingRecyclerViewUtils;

/**
 * Created by takuasahina on 2018/02/27.
 */

public class FavoriteListFragment extends Fragment {

	public static FavoriteListFragment newInstance() {
		FavoriteListFragment fragment = new FavoriteListFragment();

		Bundle args = new Bundle();

		fragment.setArguments(args);
		return fragment;
	}


	private FavoriteListViewModel viewModel;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

		final FragmentFavoriteListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite_list, container, false);
		viewModel = new FavoriteListViewModel();
		binding.setViewModel(viewModel);
		binding.setView(this);


		DataBindingRecyclerViewUtils.bindSortable(binding.recyclerView, viewModel.movieItemList, BR.viewModel, R.layout.item_movie_list,
				R.id.swap,
				new Consumer<Pair<Integer, MovieItem>>() {
					@Override
					public void accept(Pair<Integer, MovieItem> pair) {
						Uri uri = Uri.parse(pair.second.url);
						Intent i = new Intent(Intent.ACTION_VIEW, uri);
						startActivity(i);
					}
				},
				new Consumer<Pair<Integer, MovieItem>>() {
					@Override
					public void accept(Pair<Integer, MovieItem> pair) {
						MovieItem item = pair.second;

						SmartDialogUtils.SingleChoiceSimpleDialog.show(
								FavoriteListFragment.this,
								item.label,
								Stream.of(MovieStatusTypeEnum.values())
										.map(new Function<MovieStatusTypeEnum, String>() {
											@Override
											public String apply(MovieStatusTypeEnum movieStatusTypeEnum) {
												return movieStatusTypeEnum.label;
											}
										})
										.collect(Collectors.<String>toList()),
								item.movieStatusTypeEnum == null ? 1 : item.movieStatusTypeEnum.get().code,
								new Consumer<Pair<Integer, String>>() {
									@Override
									public void accept(Pair<Integer, String> ItemPair) {
										MovieStatusTypeEnum movieStatus = MovieStatusTypeEnum.values()[ItemPair.first];
										switch (movieStatus) {
											case LINE_1:
											case LINE_2:
												viewModel.movieItemList.get(pair.first).setMovieStatusTypeEnum(movieStatus);
												viewModel.setMovieItemList();
												break;
											case DEFAULT:
												viewModel.movieItemList.get(pair.first).setMovieStatusTypeEnum(movieStatus);
												viewModel.addMovieList(item);
											case DELETE:
												viewModel.removeItem(pair.first);
												break;
										}
									}
								});
					}
				});


		return binding.getRoot();
	}

	@Override
	public void onStart() {
		super.onStart();
		viewModel.addPropertyChangeListenerToModel();
	}

	@Override
	public void onStop() {
		super.onStop();
		viewModel.removePropertyChangeListenerFromModel();
	}
}
