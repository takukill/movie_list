package com.movie.asahina.movie.ui.main.favorite;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.movie.asahina.movie.BaseApplication;
import com.movie.asahina.movie.model.MovieModel;
import com.movie.asahina.movie.model.base.PropertyChangeListener;
import com.movie.asahina.movie.preferences.dto.MovieItem;
import com.movie.asahina.movie.utils.ListUtils;
import com.movie.asahina.movie.utils.ObservableListUtils;

import javax.inject.Inject;

/**
 * Created by takuasahina on 2018/02/27.
 */

public class FavoriteListViewModel {

	public FavoriteListViewModel() {
		BaseApplication.getComponent().inject(this);

		movieItemList.addAll(movieModel.getFavoriteList());

		movieItemList.addOnListChangedCallback(new ObservableListUtils.OnItemMovedCallback<MovieItem>() {
			@Override
			public void onMoved(int fromPosition, int toPosition) {
				movieModel.setFavoriteList(movieItemList);
			}
		});
	}


	@Inject
	MovieModel movieModel;


	public final ObservableList<MovieItem> movieItemList = new ObservableArrayList<>();


	public void refresh() {
		ListUtils.replace(movieItemList, movieModel.getFavoriteList());
	}

	public void addMovieList(MovieItem item) {
		movieModel.addMovieList(item);
	}

	public void removeItem(int position) {
		movieModel.removeFavoriteItem(position);
	}

	public void setMovieItemList() {
		movieModel.setFavoriteList(movieItemList);
	}


	//region PropertyChangeListener
	public void addPropertyChangeListenerToModel() {
		addMovieModelPropertyChangeListenerToModel();
	}

	public void removePropertyChangeListenerFromModel() {
		removeMovieModelPropertyChangeListenerFromModel();
	}

	//region homeModelPropertyChangeListener
	private PropertyChangeListener homeModelPropertyChangeListener;

	private void addMovieModelPropertyChangeListenerToModel() {
		if (homeModelPropertyChangeListener != null) {
			return;
		}

		homeModelPropertyChangeListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(String propertyName) {
				switch (propertyName) {
					case MovieModel.KEY_FAVORITE_LIST:
						refresh();
						break;
				}
			}
		};
		movieModel.addPropertyChangeListener(homeModelPropertyChangeListener);
	}

	private void removeMovieModelPropertyChangeListenerFromModel() {
		if (homeModelPropertyChangeListener == null) {
			return;
		}

		movieModel.removePropertyChangeListener(homeModelPropertyChangeListener);
		homeModelPropertyChangeListener = null;
	}
	//endregion
}
