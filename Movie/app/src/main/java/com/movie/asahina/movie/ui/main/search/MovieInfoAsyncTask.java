package com.movie.asahina.movie.ui.main.search;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by takuasahina on 2018/03/12.
 */

public class MovieInfoAsyncTask extends AsyncTask<String, Void, String> {

	private Delegate delegate;

	public MovieInfoAsyncTask(Delegate delegate) {
		this.delegate = delegate;
	}

	@Override
	protected String doInBackground(String... url) {
		try {
			Document document = Jsoup.connect(url[0]).get();
			String rating = document.select("span[itemprop = ratingValue]").text();
			String thumbnail = document.getElementsByClass("thumbnail__figure").select("p").get(0).attr("style");

			delegate.rating(rating);
			delegate.thumbnail(thumbnail.replace("background-image:url(  ", "").replace(")", ""));
		} catch (Exception e) {
		}
		return null;
	}


	// このメソッドは非同期処理の終わった後に呼び出されます
	@Override
	protected void onPostExecute(String result) {
	}

	interface Delegate {

		void rating(String rating);

		void thumbnail(String thumbnail);

	}
}
