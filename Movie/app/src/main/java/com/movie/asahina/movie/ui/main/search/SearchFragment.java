package com.movie.asahina.movie.ui.main.search;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.movie.asahina.movie.R;
import com.movie.asahina.movie.databinding.FragmentSearchBinding;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Created by takuasahina on 2018/02/27.
 */

public class SearchFragment extends Fragment implements SearchViewModel.Delegate {

	public static SearchFragment newInstance() {
		SearchFragment fragment = new SearchFragment();

		Bundle args = new Bundle();

		fragment.setArguments(args);
		return fragment;
	}


	private FragmentSearchBinding binding;

	private SearchViewModel viewModel;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
		viewModel = new SearchViewModel(this);
		binding.setViewModel(viewModel);
		binding.setView(this);

		binding.webView.getSettings().setJavaScriptEnabled(false);
		binding.webView.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				viewModel.load.set(true);
			}

			public void onPageFinished(WebView view, String url) {
				viewModel.urlText.set(url);
				viewModel.removeParam();
			}
		});

		binding.searchEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {

				if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
					viewModel.selectSearch();
					return true;
				}

				return false;
			}
		});

		return binding.getRoot();
	}


	public void onBackPressed() {
		if (binding.webView.canGoBack()) {
			binding.webView.goBack();
			return;
		}

		getActivity().finish();
	}


	@Override
	public void selectSearch() {
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(binding.searchEdit.getWindowToken(), 0);

		binding.webView.loadUrl("https://movies.yahoo.co.jp/search/?p=" + viewModel.searchText.get());
	}

	@Override
	public void selectRegister() {
		Toast.makeText(getContext(), "登録", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void showToast() {
		Toast.makeText(getContext(), "登録済み", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void getMovieInfo() {
		viewModel.load.set(true);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Document document = Jsoup.connect(viewModel.urlText.get()).get();

					// レーティング
					viewModel.rating.set(document.select("span[itemprop = ratingValue]").text());

					// タイトル・製作年
					Elements elementClass = document.getElementsByClass("movie_data");
					String title = elementClass.select("span[itemprop = name]").text().split(" ")[0];
					String year = document.getElementsByClass("movie_data").select("small[class = text-small]").text().replace("(", "").replace(")", "");
					viewModel.title.set(title);
					viewModel.year.set(year);

					// サムネイル
					String thumbnail = document.getElementsByClass("thumbnail__figure").select("p").get(0).attr("style");
					viewModel.thumbnail.set(thumbnail.replace("background-image:url(  ", "").replace(")", ""));
				} catch (Exception e) {
				}

				viewModel.load.set(false);
			}
		}).start();
	}
}
