package com.movie.asahina.movie.ui.main.search;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.movie.asahina.movie.BaseApplication;
import com.movie.asahina.movie.model.MovieModel;
import com.movie.asahina.movie.preferences.dto.MovieItem;

import javax.inject.Inject;

/**
 * Created by takuasahina on 2018/02/27.
 */

public class SearchViewModel {

	public SearchViewModel(Delegate delegate) {
		BaseApplication.getComponent().inject(this);

		this.delegate = delegate;

		this.urlText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
			@Override
			public void onPropertyChanged(Observable observable, int i) {
				if (urlText.get().startsWith("https://movies.yahoo.co.jp/movie/")) {
					delegate.getMovieInfo();
				}
			}
		});

		this.title.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
			@Override
			public void onPropertyChanged(Observable observable, int i) {
				register.set(true);
			}
		});
	}


	@Inject
	MovieModel movieModel;


	public final ObservableField<String> searchText = new ObservableField<>();

	public final ObservableField<String> title = new ObservableField<>();

	public final ObservableField<String> urlText = new ObservableField<>();

	public final ObservableField<String> rating = new ObservableField<>();

	public final ObservableField<String> year = new ObservableField<>();

	public final ObservableField<String> thumbnail = new ObservableField<>();

	public final ObservableBoolean register = new ObservableBoolean();

	public final ObservableBoolean load = new ObservableBoolean();


	public void selectSearch() {
		delegate.selectSearch();
	}

	public void removeParam() {
		rating.set(null);
		title.set(null);
		year.set(null);
		thumbnail.set(null);
		load.set(false);
	}

	public void selectResister() {
		if (TextUtils.isEmpty(title.get())) {
			return;
		}

		MovieItem item = Stream.of(movieModel.getMovieList())
				.filter(new Predicate<MovieItem>() {
					@Override
					public boolean test(MovieItem value) {
						return value.label.equals(title.get()) && value.url.equals(urlText.get());
					}
				})
				.findFirst()
				.orElse(null);

		if (item != null) {
			delegate.showToast();
			return;
		}


		movieModel.addMovieList(new MovieItem(title.get(), urlText.get(), year.get(), rating.get(), thumbnail.get()));
		delegate.selectRegister();
	}


	private final Delegate delegate;

	public interface Delegate {

		void selectSearch();

		void selectRegister();

		void showToast();

		void getMovieInfo();

	}
}
