package com.movie.asahina.movie.utils;

import android.text.TextUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by takuasahina on 2017/11/20.
 */

public class FormatUtils {

	public static String formatThousand(int price) {
		return formatThousand(String.valueOf(price));
	}

	public static String formatThousand(String price) {
		String thousandYen = new BigDecimal(price).divide(new BigDecimal("10000")).toPlainString();

		if (thousandYen.indexOf(".") == -1) {
			thousandYen += ".0";
		}

		return thousandYen;
	}

	public static String[] formatThousandYen(int price) {
		return formatThousand(price).split("\\.");
	}

	public static String[] formatThousandYen(String price) {
		return formatThousandYen(Integer.parseInt(price));
	}

	public static String formatThousandYenFirst(int price) {
		return formatThousandYen(price)[0];
	}

	public static String formatThousandYenFirst(String price) {
		return formatThousandYen(price)[0];
	}

	public static String formatThousandYenSecond(int price) {
		return formatThousandYen(price)[1];
	}

	public static String formatThousandYenSecond(String price) {
		return formatThousandYen(price)[1];
	}

	public static String formatLikeCount(Integer likeCount) {
		if (likeCount == null) {
			return "0";
		}

		if (new BigDecimal(likeCount).compareTo(BigDecimal.TEN.multiply(BigDecimal.TEN)) < 0) {
			// likeCount < 100
			return String.valueOf(likeCount);
		} else {
			return "99+";
		}
	}

	public static String formatMileage(Integer mileage) {
		if (mileage == null) {
			return "";
		}

		return new BigDecimal(mileage).divide(new BigDecimal("10000"), 1, RoundingMode.DOWN).toString() + "万km";
	}

	public static String formatMileage(String mileage) {
		if (mileage == null) {
			return "";
		}

		return new BigDecimal(mileage).divide(new BigDecimal("10000"), 1, RoundingMode.DOWN).toString() + "万km";
	}

	public static String formatDate_YYdd(int dateNumber) {
		String date = String.valueOf(dateNumber);
		return date.substring(2, 4) + "/" + date.substring(4, 6);
	}

	public static String formatDate_YYYYMM(Integer dateNumber) {
		if (dateNumber == null) {
			return "なし";
		}

		String date = String.valueOf(dateNumber);
		return date.substring(0, 4) + "/" + (Integer.parseInt(date.substring(4, 6)));
	}

	public static String formatDate_YYYYMMDD(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}

		String[] split = date.split(" ");
		String[] dateSplit = split[0].split("-");
		return dateSplit[0] + "年" + dateSplit[1] + "月" + dateSplit[2] + "日";
	}

	public static String formatDate_YYYYMMDD_Slash(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}

		String[] split = date.split(" ");
		String[] dateSplit = split[0].split("-");
		return dateSplit[0] + "/" + dateSplit[1] + "/" + dateSplit[2];
	}

	public static String formatDate_MMDD(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}

		String[] split = date.split(" ");
		String[] dateSplit = split[0].split("-");
		return dateSplit[1] + "/" + dateSplit[2];
	}

	public static String formatDate_YYYYMMDDHHMM(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}

		String[] split = date.split(" ");
		String[] dateSplit = split[0].split("-");
		String[] timeSplit = split[1].split(":");
		return dateSplit[0] + "年" + dateSplit[1] + "月" + dateSplit[2] + "日" + " " + timeSplit[0] + ":" + timeSplit[1];
	}

	public static String formatNumber(Integer number) {
		return formatNumber(String.valueOf(number));
	}

	public static String formatNumber(String number) {
		String integers;
		String decimals;

		if (TextUtils.isEmpty(number)) {
			return "";
		}

		if (number.indexOf(".") < 0) {
			integers = number.replaceAll("\\d(?=(\\d{3})+$)", "$0,");
			decimals = "";
		} else {
			String[] parts = number.split("\\.");
			integers = parts[0].replaceAll("\\d(?=(\\d{3})+$)", "$0,");
			decimals = "." + parts[1];
		}

		return integers + decimals;
	}

	public static String formatTwoDigit(int value) {
		String time = String.valueOf(value);
		if (time.length() == 1) {
			time += "0" + time;
		}
		return time;
	}
}
