package com.movie.asahina.movie.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;

/**
 * Created by fxgmo on 2017/10/05.
 */

public class PermissionUtils {

	public static boolean checkPermission(final Activity activity, int requestPermission, String... permission) {

		if (Build.VERSION.SDK_INT < 23) {
			return true;
		}

		String checkPermission = Stream.of(permission)
				.filter(new Predicate<String>() {
					@Override
					public boolean test(String value) {
						return ContextCompat.checkSelfPermission(activity, value) != PackageManager.PERMISSION_GRANTED;
					}
				})
				.findFirst()
				.orElse(null);

		if (!TextUtils.isEmpty(checkPermission)) {
			ActivityCompat.requestPermissions(activity, permission, requestPermission);
			return false;
		}

		return true;
	}
}
