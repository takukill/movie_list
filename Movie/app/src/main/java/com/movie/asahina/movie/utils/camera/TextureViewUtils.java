package com.movie.asahina.movie.utils.camera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.support.annotation.NonNull;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import java.util.Arrays;

/**
 * Created by takuasahina on 2017/11/07.
 */

@SuppressLint("NewApi, MissingPermission")
public class TextureViewUtils implements TextureView.SurfaceTextureListener {

	public TextureViewUtils(Activity activity, CameraUtils.CameraListener cameraListener) {
		this.activity = activity;

		this.cameraListener = cameraListener;
	}


	//region textureView param
	private final Activity activity;

	private final CameraUtils.CameraListener cameraListener;

	private CaptureRequest.Builder builder;

	private CameraDevice cameraDevice;
	//endregion

	public void openCamera(TextureView textureView) {

		textureView.setSurfaceTextureListener(this);
	}

	public void closeCamera() {
		if (cameraDevice == null) {
			return;
		}

		cameraDevice.close();
	}


	//region textureView
	@Override
	public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, int i, int i1) {

		final Surface surface = new Surface(surfaceTexture);

		try {
			CameraManager cameraManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
			for (String cameraId : cameraManager.getCameraIdList()) {
				CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
				if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {

					// Mapを取得
					StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

					// カメラサイズ0番目をGlobalな関数に格納
					final Size cameraSize = map.getOutputSizes(SurfaceTexture.class)[0];

					// カメラをオープン
					cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {

						@Override
						public void onOpened(@NonNull CameraDevice device) {
							cameraDevice = device;

							try {
								//captureRequestの生成
								builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
							} catch (CameraAccessException e) {
								e.printStackTrace();
							}

							// Requestに取得したSurfaceを設定
							builder.addTarget(surface);

							// SurfaceTextureにサイズを設定
							surfaceTexture.setDefaultBufferSize(cameraSize.getWidth(), cameraSize.getHeight());

							// CaptureSessionの生成
							try {
								cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {

									@Override
									public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
										try {
											cameraCaptureSession.setRepeatingRequest(builder.build(), new CameraCaptureSession.CaptureCallback() {
												@Override
												public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
													super.onCaptureCompleted(session, request, result);
												}
											}, null);
										} catch (CameraAccessException e) {
											e.printStackTrace();
										}
									}

									@Override
									public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

									}

								}, null);
							} catch (CameraAccessException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onDisconnected(@NonNull CameraDevice cameraDevice) {

						}

						@Override
						public void onError(@NonNull CameraDevice cameraDevice, int i) {

						}
					}, null);
				}

			}
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
		return false;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

	}
	//endregion
}
