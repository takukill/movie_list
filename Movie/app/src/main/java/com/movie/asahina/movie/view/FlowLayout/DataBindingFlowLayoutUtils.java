package com.movie.asahina.movie.view.FlowLayout;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.annimon.stream.function.Consumer;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/12/01.
 */

public class DataBindingFlowLayoutUtils {

	public static <T> void bind(FlowLayout flowLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId) {
		new SimpleFlowLayoutAdapter<>(flowLayout, itemCollection, variableId, layoutId, null);
	}

	public static <T> void bind(FlowLayout flowLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		new SimpleFlowLayoutAdapter<>(flowLayout, itemCollection, variableId, layoutId, onItemClick);
	}
}
