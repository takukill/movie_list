package com.movie.asahina.movie.view.FlowLayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.movie.asahina.movie.R;

/**
 * Created by takuasahina on 2017/12/01.
 */

public class FlowLayout extends ViewGroup {

	public FlowLayout(Context context) {
		super(context);
	}

	public FlowLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FlowLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.getTheme().obtainStyledAttributes(
				attrs,
				R.styleable.FlowLayout,
				0,
				0);

		try {
			allChildrenSameSize = a.getBoolean(R.styleable.FlowLayout_allChildrenSameSize, false);
			minMargin = a.getDimensionPixelSize(R.styleable.FlowLayout_minMargin, 0);
		} finally {
			a.recycle();
		}
	}


	private boolean allChildrenSameSize;

	private int minMargin;


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
		int drawableWidth;
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
			drawableWidth = sizeWidth - getPaddingStart() - getPaddingEnd();
		} else {
			drawableWidth = sizeWidth - getPaddingLeft() - getPaddingRight();
		}

		int childLeft = 0;
		int childTop = 0;
		int lineHeight = 0;

		int childCount = getChildCount();

		for (int i = 0; i < childCount; i++) {
			View child = getChildAt(i);

			if (child.getVisibility() == GONE) {
				continue;
			}

			child.measure(getChildMeasureSpec(widthMeasureSpec, 0, child.getLayoutParams().width), getChildMeasureSpec(heightMeasureSpec, 0, child.getLayoutParams().height));

			int childWidth = child.getMeasuredWidth();
			int childHeight = child.getMeasuredHeight();

			lineHeight = Math.max(childHeight, lineHeight);
			if (childLeft + childWidth > drawableWidth) {
				childLeft = 0;
				childTop += lineHeight + minMargin;
				lineHeight = childHeight;
			}

			childLeft += childWidth + minMargin;
		}

		int wantedHeight = childTop + lineHeight + getPaddingTop() + getPaddingBottom();
		setMeasuredDimension(sizeWidth, wantedHeight);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		int sizeWidth = r - l;
		int drawableWidth;
		int paddingLeft;
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
			drawableWidth = sizeWidth - getPaddingStart() - getPaddingEnd();
			paddingLeft = getPaddingStart();
		} else {
			drawableWidth = sizeWidth - getPaddingLeft() - getPaddingRight();
			paddingLeft = getPaddingLeft();
		}
		int paddingTop = getPaddingTop();

		int childLeft = 0;
		int childTop = 0;
		int lineHeight = 0;

		int childCount = getChildCount();

		if (allChildrenSameSize && childCount > 0) {
			View firstChild = getChildAt(0);

			int childWidth = firstChild.getMeasuredWidth();
			int childHeight = firstChild.getMeasuredHeight();

			int lineChildCount = (int) Math.floor((drawableWidth - childWidth) / (minMargin + childWidth)) + 1;
			int margin = Math.round((drawableWidth - (lineChildCount * childWidth)) / (lineChildCount - 1));
			margin = Math.max(margin, minMargin);

			for (int i = 0; i < childCount; i++) {
				View child = getChildAt(i);

				if (child.getVisibility() == GONE) {
					continue;
				}

				if (childLeft + childWidth > drawableWidth) {
					childLeft = 0;
					childTop += childHeight + minMargin;
				}

				child.layout(
						paddingLeft + childLeft,
						paddingTop + childTop,
						paddingLeft + childLeft + childWidth,
						paddingTop + childTop + childHeight);

				childLeft += childWidth + margin;
			}
		} else {
			for (int i = 0; i < childCount; i++) {
				View child = getChildAt(i);

				if (child.getVisibility() == GONE) {
					continue;
				}

				int childWidth = child.getMeasuredWidth();
				int childHeight = child.getMeasuredHeight();

				lineHeight = Math.max(childHeight, lineHeight);

				if (childLeft + childWidth > drawableWidth) {
					childLeft = 0;
					childTop += lineHeight + minMargin;
					lineHeight = childHeight;
				}

				child.layout(
						paddingLeft + childLeft,
						paddingTop + childTop,
						paddingLeft + childLeft + childWidth,
						paddingTop + childTop + childHeight);

				childLeft += childWidth + minMargin;
			}
		}
	}
}
