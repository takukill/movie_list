package com.movie.asahina.movie.view.FlowLayout;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/11/10.
 */

public class SimpleFlowLayoutAdapter<T> implements View.OnClickListener {

	public SimpleFlowLayoutAdapter(final FlowLayout flowLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable final Consumer<Pair<Integer, T>> onItemClick) {
		super();

		context = flowLayout.getContext();

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		variableLayoutPair = new VariableLayoutPair(variableId, layoutId);

		if (itemCollection instanceof ObservableList) {
			itemList = (ObservableList<T>) itemCollection;
		} else {
			itemList = new ObservableArrayList<>();
			itemList.addAll(itemCollection);
		}

		this.onItemClick = onItemClick;

		itemList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<T>>() {
			@Override
			public void onChanged(ObservableList<T> sender) {
				invalidateView(flowLayout);
			}

			@Override
			public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
				invalidateView(flowLayout);
			}

			@Override
			public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
				invalidateView(flowLayout);
			}

			@Override
			public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
				invalidateView(flowLayout);
			}

			@Override
			public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
				invalidateView(flowLayout);
			}
		});
	}

	private void invalidateView(final FlowLayout flowLayout) {
		flowLayout.removeAllViews();

		Stream.of(itemList)
				.forEach(new Consumer<T>() {
					@Override
					public void accept(T itemViewModel) {
						ViewDataBinding binding = DataBindingUtil.inflate(inflater, variableLayoutPair.layoutId, null, false);
						binding.setVariable(variableLayoutPair.variableId, itemViewModel);

						View view = binding.getRoot();

						if (onItemClick != null) {
							view.setTag(R.id.key_simple_grid_view_adapter_position, flowLayout.getChildCount());
							view.setTag(R.id.key_simple_grid_view_adapter_item, itemViewModel);
							view.setOnClickListener(SimpleFlowLayoutAdapter.this);
						}

						flowLayout.addView(view);
					}
				});

		flowLayout.invalidate();
	}


	private final Context context;

	private final LayoutInflater inflater;

	private final VariableLayoutPair variableLayoutPair;

	private final ObservableList<T> itemList;

	private final Consumer<Pair<Integer, T>> onItemClick;

	@Override
	public void onClick(View v) {
		if (onItemClick == null) {
			return;
		}

		onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_grid_view_adapter_position), (T) v.getTag(R.id.key_simple_grid_view_adapter_item)));
	}
}