package com.movie.asahina.movie.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by takuasahina on 2018/01/25.
 */

public class LinearLayoutDetectsSoftKeyboard extends LinearLayout {

	public LinearLayoutDetectsSoftKeyboard(Context context, AttributeSet attrs) {
		super(context, attrs);
	}


	private boolean keyboardOpen = false;


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (listener != null) {
			int height = MeasureSpec.getSize(heightMeasureSpec);
			Activity activity = (Activity) getContext();
			Rect rect = new Rect();
			activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
			int statusBarHeight = rect.top;
			int screenHeight = activity.getWindowManager().getDefaultDisplay().getHeight();
			int diff = (screenHeight - statusBarHeight) - height;

			boolean isOpen = diff > 128;

			if (keyboardOpen != isOpen) {
				listener.onSoftKeyboardChanged(isOpen);
				keyboardOpen = isOpen;
			}

		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}


	public void setKeyboardOpenListener(OnSoftKeyboardChanged listener) {
		this.listener = listener;
	}

	public boolean isKeyboardOpen() {
		return keyboardOpen;
	}


	private OnSoftKeyboardChanged listener;

	public interface OnSoftKeyboardChanged {

		void onSoftKeyboardChanged(boolean isOpen);
	}
}
