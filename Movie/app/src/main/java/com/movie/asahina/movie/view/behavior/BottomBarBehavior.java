package com.movie.asahina.movie.view.behavior;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import java.math.BigDecimal;

/**
 * Created by takuasahina on 2017/11/09.
 */

public class BottomBarBehavior extends CoordinatorLayout.Behavior<View> {

	private int defaultDependencyTop = -1;

	private float scale;

	public BottomBarBehavior(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean layoutDependsOn(CoordinatorLayout parent, View bottomBar, View dependency) {
		return dependency instanceof AppBarLayout;
	}

	@Override
	public boolean onDependentViewChanged(CoordinatorLayout parent, View bottomBar, View dependency) {
		if (defaultDependencyTop == -1) {
			scale = new BigDecimal(bottomBar.getHeight()).divide((new BigDecimal(dependency.getHeight())), 4, BigDecimal.ROUND_HALF_UP).floatValue();
			defaultDependencyTop = dependency.getTop();
		}

		ViewCompat.setTranslationY(bottomBar, (-dependency.getTop() + defaultDependencyTop) * scale);
		return true;
	}
}