package com.movie.asahina.movie.view.circleImageView;

import android.content.Context;
import android.util.AttributeSet;

public class SmartCircleImageView extends de.hdodenhof.circleimageview.CircleImageView {

	public SmartCircleImageView(Context context) {
		super(context);
	}

	public SmartCircleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SmartCircleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}