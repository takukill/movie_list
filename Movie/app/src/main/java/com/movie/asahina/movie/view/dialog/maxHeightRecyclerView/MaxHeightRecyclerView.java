package com.movie.asahina.movie.view.dialog.maxHeightRecyclerView;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.movie.asahina.movie.R;

/**
 * Created by takuasahina on 2017/12/26.
 */

public class MaxHeightRecyclerView extends RecyclerView {

	private int maxHeight;

	private final int defaultHeight = 400;

	public MaxHeightRecyclerView(Context context) {
		super(context);
	}

	public MaxHeightRecyclerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			init(context, attrs);
		}
	}

	public MaxHeightRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		if (!isInEditMode()) {
			init(context, attrs);
		}
	}

	private void init(Context context, AttributeSet attrs) {
		if (attrs != null) {
			TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.maxHeight);
			//200 is a defualt value
			maxHeight = styledAttrs.getDimensionPixelSize(R.styleable.maxHeight_maxHeight, defaultHeight);
			styledAttrs.recycle();
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}