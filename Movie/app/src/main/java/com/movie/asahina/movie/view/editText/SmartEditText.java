package com.movie.asahina.movie.view.editText;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by takuasahina on 2017/12/14.
 */

public class SmartEditText extends android.support.v7.widget.AppCompatEditText {

	public SmartEditText(Context context) {
		super(context);
		init(context);
	}

	public SmartEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SmartEditText(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	private void init(final Context context) {
		this.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					// フォーカスが外れた場合キーボードを非表示にする
					InputMethodManager inputMethodMgr = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
					inputMethodMgr.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				}
			}
		});
	}
}
