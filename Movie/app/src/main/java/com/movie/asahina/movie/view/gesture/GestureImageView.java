package com.movie.asahina.movie.view.gesture;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * Created by takuasahina on 2017/11/20.
 */

public class GestureImageView extends android.support.v7.widget.AppCompatImageView {

	public GestureImageView(Context context) {
		super(context);
		init(context);
	}

	public GestureImageView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		init(context);
	}

	public GestureImageView(Context context, AttributeSet attributeSet, int i) {
		super(context, attributeSet, i);
		init(context);
	}


	private TouchMode touchMode = TouchMode.NONE;

	private Matrix baseMatrix = new Matrix();

	private Matrix imgMatrix = new Matrix();

	private PointF point = new PointF();

	private ScaleGestureDetector gesDetect = null;

	private float scaleFactor = 100;

	private final int MIN_SIZE = 20;


	private void init(Context context) {
		setScaleType(ScaleType.MATRIX);
		gesDetect = new ScaleGestureDetector(context, onScaleGestureListener);
	}


	@Override
	public void setImageBitmap(Bitmap bitmap) {

		float imageWidth = bitmap.getWidth();
		float imageHeight = bitmap.getHeight();
		RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
		RectF viewRect = new RectF(0, 0, this.getWidth(), this.getHeight());
		this.imgMatrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
		this.setImageMatrix(imgMatrix);

		super.setImageBitmap(bitmap);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		int count = event.getPointerCount();

		switch (action) {
			case MotionEvent.ACTION_DOWN:
				if (touchMode == TouchMode.NONE && count == TouchMode.TOUCH_SINGLE.count) {
					point.set(event.getX(), event.getY());
					baseMatrix.set(imgMatrix);
					touchMode = TouchMode.TOUCH_SINGLE;
				}
				break;

			case MotionEvent.ACTION_MOVE:
				if (touchMode == TouchMode.TOUCH_SINGLE) {
					imgMatrix.set(baseMatrix);
					imgMatrix.postTranslate(event.getX() - point.x, event.getY() - point.y);

				}
				break;

			case MotionEvent.ACTION_UP:
				if (touchMode == TouchMode.TOUCH_SINGLE) {
					touchMode = TouchMode.NONE;
				}
				break;
		}

		if (count >= TouchMode.TOUCH_MULTI.count) {
			gesDetect.onTouchEvent(event);
		}

		setImageMatrix(imgMatrix);

		return true;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (this.getDrawable() == null) {
			return;
		}

		float imageWidth = this.getDrawable().getIntrinsicWidth();
		float imageHeight = this.getDrawable().getIntrinsicHeight();
		RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
		RectF viewRect = new RectF(0, 0, this.getWidth(), this.getHeight());
		imgMatrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
		this.setImageMatrix(imgMatrix);
		this.invalidate();
	}


	private final ScaleGestureDetector.SimpleOnScaleGestureListener onScaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {

		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			baseMatrix.set(imgMatrix);
			touchMode = TouchMode.TOUCH_MULTI;
			return super.onScaleBegin(detector);
		}

		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			if (scaleFactor * detector.getScaleFactor() > MIN_SIZE) {
				imgMatrix.set(baseMatrix);
				imgMatrix.postScale(detector.getScaleFactor(), detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
			}
			return super.onScale(detector);
		}

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {
			touchMode = TouchMode.NONE;

			if (scaleFactor * detector.getScaleFactor() > MIN_SIZE) {
				scaleFactor = scaleFactor * detector.getScaleFactor();
			} else {
				scaleFactor = MIN_SIZE;
			}
			super.onScaleEnd(detector);
		}
	};

	public enum TouchMode {
		NONE(0, "なし", 0),
		TOUCH_SINGLE(1, "移動", 1),
		TOUCH_MULTI(2, "拡大・縮小", 2);

		TouchMode(int code, String label, int count) {
			this.code = code;
			this.label = label;
			this.count = count;
		}


		public final int code;

		public final String label;

		public final int count;
	}
}