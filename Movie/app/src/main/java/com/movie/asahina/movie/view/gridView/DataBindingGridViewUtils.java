package com.movie.asahina.movie.view.gridView;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.widget.GridView;

import com.annimon.stream.function.Consumer;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/11/09.
 */

public class DataBindingGridViewUtils {

	public static <T> void bind(GridView gridView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId) {
		SimpleGridViewAdapter<T> adapter = new SimpleGridViewAdapter<>(gridView, itemCollection, variableId, layoutId, null);
		gridView.setAdapter(adapter);
	}

	public static <T> void bind(GridView gridView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		SimpleGridViewAdapter<T> adapter = new SimpleGridViewAdapter<>(gridView, itemCollection, variableId, layoutId, onItemClick);
		gridView.setAdapter(adapter);
	}
}
