package com.movie.asahina.movie.view.linearLayout;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.widget.LinearLayout;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * Created by takuasahina on 2018/02/13.
 */

public class LinearLayoutUtils {

	public static <T> void bind(LinearLayout linearLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		SimpleLinearLayoutAdapter<T> adapter = new SimpleLinearLayoutAdapter<>(linearLayout, itemCollection, new VariableLayoutPair(variableId, layoutId), onItemClick, onItemLongClick);
		adapter.addView();
	}

	public static <T> void bind(LinearLayout linearLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bind(linearLayout, itemCollection, variableId, layoutId, onItemClick, null);
	}

	public static <T> void bind(LinearLayout linearLayout, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId) {
		bind(linearLayout, itemCollection, variableId, layoutId, null, null);
	}
}
