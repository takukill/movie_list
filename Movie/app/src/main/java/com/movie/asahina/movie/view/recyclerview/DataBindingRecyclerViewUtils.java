package com.movie.asahina.movie.view.recyclerview;

import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.view.Utils.ScrollEvent;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;
import java.util.List;

/**
 * データバインディング対応 RecyclerViewAdapter 用のユーティリティ。
 */
public class DataBindingRecyclerViewUtils {

	//region SimpleRecyclerViewAdapter
	public static <T> SimpleRecyclerViewAdapter<T> bindAdapter(Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		return new SimpleRecyclerViewAdapter<>(itemCollection, new VariableLayoutPair(variableId, layoutId), onItemClick, onItemLongClick);
	}

	public static <T> void bindScroll(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick, ScrollEvent scrollEvent) {
		bind(recyclerView, itemCollection, variableId, layoutId, onItemClick, onItemLongClick);
		recyclerView.addOnScrollListener(new SimpleScrollListener(scrollEvent));
	}

	public static <T> void bindScroll(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, ScrollEvent scrollEvent) {
		bind(recyclerView, itemCollection, variableId, layoutId, onItemClick, null);
		recyclerView.addOnScrollListener(new SimpleScrollListener(scrollEvent));
	}

	public static <T> void bind(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		SimpleRecyclerViewAdapter<T> adapter = new SimpleRecyclerViewAdapter<>(itemCollection, new VariableLayoutPair(variableId, layoutId), onItemClick, onItemLongClick);
		recyclerView.setItemViewCacheSize(7);
		recyclerView.setAdapter(adapter);
	}

	public static <T> void bind(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bind(recyclerView, itemCollection, variableId, layoutId, onItemClick, null);
	}

	public static <T> void bind(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId) {
		bind(recyclerView, itemCollection, variableId, layoutId, null, null);
	}
	//endregion

	//region SortableRecyclerViewAdapter
	public static <T> void bindSortable(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @IdRes int sortingHandlerViewId, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		SortableRecyclerViewAdapter<T> adapter = new SortableRecyclerViewAdapter<>(itemCollection, new VariableLayoutPair(variableId, layoutId), sortingHandlerViewId, onItemClick, onItemLongClick);
		recyclerView.setAdapter(adapter);
	}

	public static <T> void bindSortable(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @IdRes int sortingHandlerViewId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bindSortable(recyclerView, itemCollection, variableId, layoutId, sortingHandlerViewId, onItemClick, null);
	}

	public static <T> void bindSortable(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @IdRes int sortingHandlerViewId) {
		bindSortable(recyclerView, itemCollection, variableId, layoutId, sortingHandlerViewId, null, null);
	}
	//endregion

	//region PagingRecyclerViewAdapter
	public static <T> void bindPaging(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, ObservableBoolean progressBarEnabled, Runnable onBottomItemAppeared, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		PagingRecyclerViewAdapter<T> adapter = new PagingRecyclerViewAdapter<>(itemCollection, new VariableLayoutPair(variableId, layoutId), progressBarEnabled, onBottomItemAppeared, onItemClick, onItemLongClick);
		recyclerView.setItemViewCacheSize(5);
		recyclerView.setAdapter(adapter);
	}

	public static <T> void bindPaging(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, ObservableBoolean progressBarEnabled, Runnable onBottomItemAppeared, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bindPaging(recyclerView, itemCollection, variableId, layoutId, progressBarEnabled, onBottomItemAppeared, onItemClick, null);
	}

	public static <T> void bindPaging(RecyclerView recyclerView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, ObservableBoolean progressBarEnabled, Runnable onBottomItemAppeared) {
		bindPaging(recyclerView, itemCollection, variableId, layoutId, progressBarEnabled, onBottomItemAppeared, null, null);
	}
	//endregion

	//region SectionedRecyclerViewAdapter
	public static RecyclerView bindSectioned(RecyclerView recyclerView, SectionedRecyclerViewAdapter.Section... sections) {
		bindSectioned(recyclerView, Stream.of(sections).collect(Collectors.<SectionedRecyclerViewAdapter.Section>toList()));
		return recyclerView;
	}

	public static RecyclerView bindSectioned(RecyclerView recyclerView, List<SectionedRecyclerViewAdapter.Section> sections) {
		SectionedRecyclerViewAdapter adapter = new SectionedRecyclerViewAdapter(sections);
		recyclerView.setAdapter(adapter);
		return recyclerView;
	}
	//endregion

	@BindingAdapter("showDivider")
	public static void setShowDivider(RecyclerView recyclerView, boolean showDivider) {
		DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), false);
		if (showDivider) {
			recyclerView.addItemDecoration(dividerItemDecoration);
		}
	}

	@BindingAdapter("showListDivider")
	public static void setShowListDivider(RecyclerView recyclerView, boolean showDivider) {
		DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), true);
		if (showDivider) {
			recyclerView.addItemDecoration(dividerItemDecoration);
		}
	}
}

