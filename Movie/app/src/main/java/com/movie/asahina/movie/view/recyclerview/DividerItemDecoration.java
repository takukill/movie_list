package com.movie.asahina.movie.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.movie.asahina.movie.R;

/**
 * RecyclerView 用 divider。
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

	public DividerItemDecoration(Context context, boolean listDivider) {
		this.listDivider = listDivider;
		TypedArray typedArray = context.obtainStyledAttributes(new int[]{android.R.attr.listDivider});
		divider = typedArray.getDrawable(0);
		typedArray.recycle();
	}

	private final Drawable divider;

	private final boolean listDivider;

	@Override
	public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
		int left = parent.getPaddingLeft();
		int right = parent.getWidth() - parent.getPaddingRight();

//		for (int i = 0; i < parent.getChildCount() - 1; i++) {
//
//			if (listDivider) {
//				if (i + 1 == parent.getChildCount()) {
//					continue;
//				}
//			}
		for (int i = 0; i < parent.getChildCount(); i++) {

			if (listDivider) {
				if (i == parent.getChildCount()) {
					continue;
				}
			}

			View child = parent.getChildAt(i);
			int top = child.getBottom() + ((RecyclerView.LayoutParams) child.getLayoutParams()).bottomMargin;
			int bottom = top + divider.getIntrinsicHeight();
			divider.setBounds(left, top, right, bottom);
			divider.draw(c);
		}
	}

	@Override
	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
		outRect.set(0, 0, 0, divider.getIntrinsicHeight());
	}

	public static class DividerItemDecorationBindingAdapter {

		private static void addDividerItemDecoration(RecyclerView recyclerView) {
			if (recyclerView.getTag(R.id.key_list_divider_item_decoration) != null) {
				return;
			}

			DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), false);
			recyclerView.addItemDecoration(itemDecoration);
			recyclerView.setTag(R.id.key_list_divider_item_decoration, itemDecoration);
		}

		private static void removeDividerItemDecoration(RecyclerView recyclerView) {
			DividerItemDecoration itemDecoration = (DividerItemDecoration) recyclerView.getTag(R.id.key_list_divider_item_decoration);
			if (itemDecoration == null) {
				return;
			}

			recyclerView.removeItemDecoration(itemDecoration);
			recyclerView.setTag(R.id.key_list_divider_item_decoration, null);
		}

		@BindingAdapter("dividerEnabled")
		public static void setDividerEnabled(RecyclerView recyclerView, boolean enabled) {
			if (enabled) {
				addDividerItemDecoration(recyclerView);
			} else {
				removeDividerItemDecoration(recyclerView);
			}
		}
	}
}