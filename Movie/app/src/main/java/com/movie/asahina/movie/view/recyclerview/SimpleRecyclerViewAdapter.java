package com.movie.asahina.movie.view.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;


/**
 * データバインディング対応 RecyclerViewAdapter。セクション分け非対応。
 * <p>
 * 参考： https://github.com/radzio/android-data-binding-recyclerview
 */
public class SimpleRecyclerViewAdapter<T> extends RecyclerView.Adapter<SimpleRecyclerViewAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public SimpleRecyclerViewAdapter(@NonNull Collection<T> itemCollection, @NonNull VariableLayoutPair variableLayoutPair, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
        onListChangedCallback = new ObservableList.OnListChangedCallback<ObservableList<T>>() {

            @Override
            public void onChanged(ObservableList<T> sender) {
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        };

        this.variableLayoutPair = variableLayoutPair;

        this.onItemClick = onItemClick;

        this.onItemLongClick = onItemLongClick;

        if (itemCollection instanceof ObservableList) {
            itemList = (ObservableList<T>) itemCollection;
        } else {
            itemList = new ObservableArrayList<>();
            itemList.addAll(itemCollection);
        }
        notifyItemRangeInserted(0, itemList.size());
        itemList.addOnListChangedCallback(onListChangedCallback);
    }

    private final ObservableList.OnListChangedCallback<ObservableList<T>> onListChangedCallback;

    private final ObservableList<T> itemList;

    private final VariableLayoutPair variableLayoutPair;

    @Nullable
    private final Consumer<Pair<Integer, T>> onItemClick;

    @Nullable
    private final Consumer<Pair<Integer, T>> onItemLongClick;

    private LayoutInflater inflater;

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        itemList.removeOnListChangedCallback(onListChangedCallback);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }

        return new ViewHolder(DataBindingUtil.inflate(inflater, viewType, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        T item = itemList.get(position);
        holder.binding.setVariable(variableLayoutPair.variableId, item);
        holder.binding.getRoot().setTag(R.id.key_simple_recycler_view_adapter_position, position);
        holder.binding.getRoot().setTag(R.id.key_simple_recycler_view_adapter_item, item);

        if (onItemClick != null) {
            holder.binding.getRoot().setOnClickListener(this);
        }

        if (onItemLongClick != null) {
            holder.binding.getRoot().setOnLongClickListener(this);
        }

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemViewType(int position) {
        return variableLayoutPair.layoutId;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onClick(View v) {
        if (onItemClick == null) {
            return;
        }

        onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), (T) v.getTag(R.id.key_simple_recycler_view_adapter_item)));
    }

    @Override
    public boolean onLongClick(View v) {
        if (onItemLongClick == null) {
            return false;
        }

        onItemLongClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), (T) v.getTag(R.id.key_simple_recycler_view_adapter_item)));
        return true;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        final ViewDataBinding binding;
    }
}