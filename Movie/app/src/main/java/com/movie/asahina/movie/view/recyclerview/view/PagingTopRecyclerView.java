package com.movie.asahina.movie.view.recyclerview.view;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;


/**
 * Created by takuasahina on 2018/01/29.
 */

public class PagingTopRecyclerView extends RecyclerView {

	private Runnable mRunnable;
	private Handler handler;

	public PagingTopRecyclerView(Context context) {
		super(context);
		init();
	}

	public PagingTopRecyclerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public PagingTopRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}


	private boolean scrollTop = true;

	private LinearLayoutManager layoutManager;

	private int firstPosition = -1;

	private int lastPosition = -1;


	private void init() {

		layoutManager = (LinearLayoutManager) getLayoutManager();

		mRunnable = new Runnable() {

			@Override
			public void run() {
				listener.onDateShow(false);
			}
		};
	}


	public void setOnScrollListener(ScrollListener listener) {

		this.listener = listener;

		listener.onDateShow(true);
		addRunnable();

		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case ACTION_MOVE:
						removeRunnable();
						break;

					case ACTION_UP:
						addRunnable();
						break;
				}
				return false;
			}
		});

		this.addOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

				int showFirstPosition = getFirstPosition();
				int showLastPosition = getLastPosition();

				if (firstPosition == showFirstPosition || lastPosition == showLastPosition) {
					return;
				}

				// 一番上のアイテムポジション
				listener.onShowFirstPosition(showFirstPosition);


				if (dy > 0) {
					// Scrolling up
					listener.onScrollItem(getLastPosition());

				} else {
					// Scrolling down
					// 表示してる最後のポジションを返す
					if (showFirstPosition <= 1) {
						if (!scrollTop) {
							// Listの一番上に来た
							listener.onTop();
						}
					} else {
						scrollTop = false;
					}

					// 表示してる最初のポジションを返す
					listener.onScrollItem(showFirstPosition);
				}

				firstPosition = showFirstPosition;
				lastPosition = showLastPosition;
			}
		});
	}


	public void addRunnable() {
		if (handler != null) {
			return;
		}

		handler = new Handler();

		handler.removeCallbacks(mRunnable);
		handler.postDelayed(mRunnable, 1 * 1000);
	}

	public void removeRunnable() {
		if (handler == null) {
			return;
		}

		listener.onDateShow(true);

		handler.removeCallbacks(mRunnable);
		handler = null;
	}

	public int getFirstPosition() {
		return layoutManager.findFirstVisibleItemPosition();
	}

	public int getLastPosition() {
		return layoutManager.findLastVisibleItemPosition();
	}

	public void checkFirstPosition() {
		if (getFirstPosition() == 0) {
			listener.onTop();
		}
	}

	public List<Integer> getVisibleItemList() {
		return Stream.range(getFirstPosition(), getLastPosition()).collect(Collectors.toList());
	}


	private ScrollListener listener;

	public interface ScrollListener {

		void onShowFirstPosition(int position);

		void onScrollItem(int position);

		void onTop();

		void onDateShow(boolean show);

	}
}
