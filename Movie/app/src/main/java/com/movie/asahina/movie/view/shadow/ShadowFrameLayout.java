package com.movie.asahina.movie.view.shadow;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.movie.asahina.movie.R;
import com.movie.asahina.movie.databinding.IncludeShadowBinding;

/**
 * Created by takuasahina on 2018/01/12.
 */

public class ShadowFrameLayout extends FrameLayout {

	public ShadowFrameLayout(@NonNull Context context) {
		super(context);
		init(context);
	}

	public ShadowFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ShadowFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		IncludeShadowBinding shadow = DataBindingUtil.inflate(inflater, R.layout.include_shadow, null, false);
		this.addView(shadow.getRoot());
	}
}
