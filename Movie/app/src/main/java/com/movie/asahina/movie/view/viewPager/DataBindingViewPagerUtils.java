package com.movie.asahina.movie.view.viewPager;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.view.Utils.ScrollEvent;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/11/09.
 */

public class DataBindingViewPagerUtils {

	public static <T> void bind(final ViewPager viewPager, final Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bind(viewPager, itemCollection, variableId, layoutId, onItemClick, null);
	}

	public static <T> void bind(final ViewPager viewPager, final Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, final ScrollEvent scrollEvent) {
		SimpleViewPagerAdapter<T> adapter = new SimpleViewPagerAdapter<>(viewPager, itemCollection, variableId, layoutId, onItemClick);
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if (scrollEvent != null) {
					scrollEvent.onScrollTop(position == 0);
					scrollEvent.onScrollBottom(position == itemCollection.size() - 1);
				}
			}

			@Override
			public void onPageSelected(int position) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
		viewPager.setAdapter(adapter);
	}

	public static <T> void bind(final ViewPager viewPager, final ViewPagerIndicator indicator, final Collection<T> itemCollection, int variableId, @LayoutRes int layoutId) {
		bind(viewPager, indicator, itemCollection, variableId, layoutId, null, null);
	}

	public static <T> void bind(final ViewPager viewPager, final ViewPagerIndicator indicator, final Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		bind(viewPager, indicator, itemCollection, variableId, layoutId, onItemClick, null);
	}

	public static <T> void bind(final ViewPager viewPager, final ViewPagerIndicator indicator, final Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick, final ScrollEvent scrollEvent) {
		SimpleViewPagerAdapter<T> adapter = new SimpleViewPagerAdapter<>(viewPager, indicator, itemCollection, variableId, layoutId, onItemClick);
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if (scrollEvent != null) {
					scrollEvent.onScrollTop(position == 0);
					scrollEvent.onScrollBottom(position == itemCollection.size() - 1);
				}
			}

			@Override
			public void onPageSelected(int position) {
				indicator.setCurrentPosition(position);
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// not
			}
		});
		viewPager.setAdapter(adapter);
		indicator.setCount(itemCollection.size());
	}
}
